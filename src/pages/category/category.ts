import { Component } from "@angular/core";
import { IonicPage, NavController, LoadingController, NavParams } from "ionic-angular";
import { AngularFireDatabase, AngularFireList } from "@angular/fire/database";
import { map } from "rxjs/operators";

@IonicPage()
@Component({
  selector: "page-category",
  templateUrl: "category.html"
})
export class CategoryPage {
  noOfItems: any;
  public Categories: Array<any> = [];
  categories: AngularFireList<any>;
  id: any;

  constructor(
    public navCtrl: NavController,
    public af: AngularFireDatabase,
    public loadingCtrl: LoadingController,    public navParams: NavParams,

  ) {
    // let loader = this.loadingCtrl.create({
    //   content: "Please wait..."
    // });
    // loader.present().then(() => {
    //   this.categories = af.list("/categories");
    //   this.categories.snapshotChanges()
    //     .pipe(
    //       map(changes =>
    //         changes.map(c => ({ $key: c.payload.key, ...c.payload.val() }))
    //       )
    //     ).subscribe((res: any) => {
    //       this.Categories = res;
    //     })
    let loader = this.loadingCtrl.create({
      content: "Please wait..."
    });
    loader.present().then(() => {
      this.id = this.navParams.get("id");
      if (this.id == undefined) {
        this.navCtrl.push("HomePage");
      }
      this.Categories = [{description:'hi',key:'1',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "beverages"},{description:'hi',key:'3',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "breads"},{description:'hi',key:'2',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Pizzeria"},{description:'hi',key:'4',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Dairy Foods"},{description:'hi',key:'5',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Snack bar"},{description:'hi',key:'6',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Bistro"},{description:'hi',key:'7',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Osteria"},{description:'hi',key:'8',thumb: "http://res.cloudinary.com/ionicfirebaseapp/image/upload/c_scale,w_400/v1483635603/shutterstock_85029121_yqrjco.jpg",
      title: "Sandwich bar"}]
      loader.dismiss();
      // .subscribe(data => {
      //   this.Categories = [];
      //   loader.dismiss();
      //   data.forEach(item => {
      //     let temp = item.payload.toJSON();
      //     temp["$key"] = item.payload.key;
      //     this.Categories.push(temp);
      //   });
      // });

    });
  }

  ionViewWillEnter() {
    let cart: Array<any> = JSON.parse(localStorage.getItem("Cart"));
    this.noOfItems = cart != null ? cart.length : null;
  }

  navigate(id) {
    this.navCtrl.push("ProductListPage", { id: id });
  }

  navcart() {
    this.navCtrl.push("CartPage");
  }
}
